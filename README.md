# [![Patreon](https://img.shields.io/badge/Patreon-Funding-inactive?style=for-the-badge&logo=patreon&color=FF424D)](https://patreon.com/kirvedx) kwaeri-node-kit-database-driver [![PayPal](https://img.shields.io/badge/PayPal-Donations-inactive?style=for-the-badge&logo=paypal&color=253B80)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

A Massively Modified Open Source Project by kirvedx

[![GPG/Keybase](https://img.shields.io/badge/GPG-1B842CB5%20Rik-inactive?style=for-the-badge&label=GnuPG2%2FKeybase&logo=gnu+privacy+guard&color=0093dd)](https://keybase.io/rik)
[![Google](https://img.shields.io/badge/Google%20Developers-kirvedx-inactive?style=for-the-badge&logo=google+tag+manager&color=414141)](https://developers.google.com/profile/u/117028112450485835638)
[![GitLab](https://img.shields.io/badge/GitLab-kirvedx-inactive?style=for-the-badge&logo=gitlab&color=fca121)](https://github.com/kirvedx)
[![GitHub](https://img.shields.io/badge/GitHub-kirvedx-inactive?style=for-the-badge&logo=github&color=181717)](https://github.com/kirvedx)
[![npm](https://img.shields.io/badge/NPM-Rik-inactive?style=for-the-badge&logo=npm&color=CB3837)](https://npmjs.com/~rik)

The @kwaeri/database-driver component for the @kwaeri/node-kit application platform

[![pipeline status](https://gitlab.com/kwaeri/node-kit/database-driver/badges/main/pipeline.svg)](https://gitlab.com/kwaeri/node-kit/database-driver/commits/main)  [![coverage report](https://gitlab.com/kwaeri/node-kit/database-driver/badges/main/coverage.svg)](https://kwaeri.gitlab.io/node-kit/database-driver/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)

## TOC
* [The Implementation](#the-implementation)
* [Getting Started](#getting-started)
  * [Installation](#installation)
  * [Include the Component Module](#include-the-component-module)
  * [Extend the class](#include-the-component)
  * [Provide a Configuration](#provide-a-configuration)
  * [Use the extended database-driver object](#use-the-extended-database-driver-object)
* [Usage](#usage)
  * [A full example with query()](#a-full-example-with-query)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

@kwaeri/database-driver reinvents the database-driver portion of the kwaeri platform.

As the database-driver component was originally baked into the nk module, its usage was entirely controlled by it. As we discern the process for decoupling the individual components which make up a kwaeri application, we'll begin to simplify the act of doing so, and provide documentation for utilizing each component individually.

## Getting Started

[@kwaeri/node-kit](https://www.npmjs.com/package/@kwaeri/node-kit) wraps the various components under the kwaeri scope necessary for building a kwaeri application, and provides a single entry point for easing the process of building a kwaeri application.

[@kwaeri/cli](https://www.npmjs.com/package/@kwaeri/cli) wraps the various CLI components under the @kwaeri scope, and provides a single entry point to the user executable framework.

However, if you wish to install @kwaeri/database-driver and utilize it specifically - perform the following steps to get started:

### Installation

Install @kwaeri/database-driver:

```bash
npm install @kwaeri/database-driver
```

### Include the Component Module

To leverage the database database-driver, you'll first need to include it:

```javascript
// INCLUDES
import {
  DatabaseConnectionBits,
  DatabaseDriver,
  QueryResult
} from '@kwaeri/database-driver';
```

### Extend the Class

As `DatabaseDriver` is an *abstract* class, an instance of it may not be created. Instead, and as a minimum requirement, you'll need to extend it.

You'll need to define a constructor which takes a configuration of type `DriverConnectionBits`, and pass the configuration in a call to `super()`. You'll also need to define the `query` method, as it too is an abstract implementation - used to mandate its presence in a `DatabaseDriver` instance, while also allowing its implementation to be controlled by the developer.

Here's the process simplified:

```typescript
class DerivedDriver extends BaseDriver {

  constructor( config: DriverConnectionBits ) {
    super( config );
  }

  query<T extends QueryResult>( query: string ): Promise<T> {

    // Implement query here
    [rows, fields] = ( query ) ? [ query, query ] : [ query, query ];

    // Obviously correct the above to resolve - its the setup for mysql2

    return Promise.resolve( { rows, fields } );
  }
};
```

### Provide a configuration

In order to instantiate an extended database-driver object, you'll need to provide a configuration of type `DriverConnectionBits`:

```typescript
const connectionBits: DriverConnectionBits = { host: "fake", port: 5000, database: "nonexistent", user: "not_real", password: "empty" };
```

#### Use the extended database-driver object

To use the extended database-driver object, you must supply the database-driver constructor with the configuration you created earlier:

```typescript
const dbo = new DerivedDriver( connectionBits );
```

## Usage

The intended usage of the @kwaeri/database-driver component module is as an abstract class, also defining the standard interface for database driver providers, for database driver providers to extend from.

### A fuller example with query()

Combining the previous section(s), and leveraging the provided `query()` method of our extended class:

```typescript
const dbo = new
    connectionBits: DriverConnectionBits = { host: "fake", port: 5000, database: "nonexistent", user: "not_real", password: "empty" },
    dbo = new Derived( connectionBits );

try {
    const result = await dbo.query( "" );
}
catch( error ) {
    return Promise.reject( new Error( `There was an error: '${error}'` ) );
}
```


**NOTE**

More documentation to come!

## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

The project also leverages Keybase for communication and alerts - outside of standard email. To join our keybase chat, run the following from terminal (assuming you have [keybase](https://www.keybase.io) installed and running):

```bash
keybase team request-access kwaeri
```

Alternatively, you could search for the team in the GUI application and request access from there.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/mmod/kwaeri-node-kit/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:incoming+mmod/kwaeri-node-kit@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/kwaeri-node-kit/issues?label_name%5B%5D=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
