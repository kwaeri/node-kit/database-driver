/**
 * SPDX-PackageName: kwaeri/database-driver
 * SPDX-PackageVersion: 0.6.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import debug from 'debug';


// DEFINES
const DEBUG = debug( 'kwaeri:database-driver' );

/**
 * @typedef { Object } HostConnectionBit
 * @property { string } host
 */
export type HostConnectionBit = {
    host: string;
}

/**
 * @typedef { Object } DatabaseConnectionBit
 * @property { string } database
 */
export type DatabaseConnectionBit = {
    database: string;
}

/**
 * @typedef { Object } UserConnectionBit
 * @property { string } user
 */
export type UserConnectionBit = {
    user: string;
}

/**
 * @typedef { Object } PasswordConnectionBit
 * @property { string } password
 */
export type PasswordConnectionBit = {
    password: string;
}

/**
 * @typedef { Object } PortConnectionBit
 * @property { number|string } port
 */
export type PortConnectionBit = {
    port?: number|string;
}

/**
 * @typedef { Object } TypeConnectionBit
 * @property { string } type
 */
export type TypeConnectionBit = {
    type?: string;
}

/**
 * @typedef { Object } DriverConnectionBits
 * @property { HostConnectionBit } host
 * @property { DatabaseConnectionBit } database
 * @property { UserConnectionBit } user
 * @property { PasswordConnectionBit } password
 * @property { PortConnectionBit } port
 * @property { TypeConnectionBit } type
 */
export type DriverConnectionBits = HostConnectionBit & DatabaseConnectionBit & UserConnectionBit & PasswordConnectionBit & PortConnectionBit & TypeConnectionBit;

/**
 * @typedef { Object } QueryResult
 * @property { Object|Array<any> } rows
 * @property { Object|Array<any> } fields
 */
export type QueryResult = {
    rows: object|Array<any>,
    fields: object|Array<any>
}


/**
 * The Database Driver interface
 */
export interface BaseDatabaseDriver {
    type: string
    connection: DriverConnectionBits
    get getType(): string
    get getConnection(): DriverConnectionBits
    query<T extends QueryResult>( queryString: string ): Promise<T>
}


/**
 * The Base Driver to inherit from for ensuring any platform
 * requirements are met
 */
export abstract class DatabaseDriver implements BaseDatabaseDriver {
    /**
     * @var { string } type
     */
    type: string;


    /**
     * @var {@link DriverConnectionBits} connection
     */
    connection: DriverConnectionBits;


    /**
     * Class constructor
     */
    constructor( config: DriverConnectionBits ) {
        // Here we must determine our DBO and make a connection
        if( !config.host )
            throw new Error( `[ERROR][DATABASE][CONSTRUCTOR]: There was an issue instantiating the database driver object. Database host value is missing from connection details.` );

        if( config.port === undefined || config.port === null ||
            ( config.port && ( ( typeof config.port == "string" && config.port === "" ) || ( typeof config.port != "number" && typeof config.port != "string" ) ) ) )
            throw new Error( `[ERROR][DATABASE][CONSTRUCTOR]: There was an issue instantiating the database driver object. Database port value is missing from connection details.` );

        if( !config.database )
            throw new Error( `[ERROR][DATABASE][CONSTRUCTOR]: There was an issue instantiating the database driver object. Database name value is missing from connection details.` );

        if( !config.user )
            throw new Error( `[ERROR][DATABASE][CONSTRUCTOR]: There was an issue instantiating the database driver object. Database user value is missing from connection details.` );

        if( !config.password )
            throw new Error( `[ERROR][DATABASE][CONSTRUCTOR]: There was an issue instantiating the database driver object. Database password value is missing from connection details.` );

        DEBUG( `Set 'type' to '${config.type}'` );

        // Set connection details
        this.type = config.type || "mysql";

        DEBUG( `Set 'connection' using 'config' values:` );

        this.connection = {
            host:       config.host,
            port:       ( typeof config.port == "string" ) ? parseInt( config.port ) : config.port,
            database:   config.database,
            user:       config.user,
            password:   config.password
        } as DriverConnectionBits;

        DEBUG( `'${this.connection}'` );
    }


    /**
     * Getter for the driver type
     *
     * @returns { string } The string type.
     */
    get getType(): string {
        return this.type;
    }


    /**
     * Getter for the connection details
     *
     * @returns { DriverConnectionBits } A {@link DriverConnectionBits} object.
     */
    get getConnection(): DriverConnectionBits {
        return this.connection;
    }


    /**
     * The query method is a required async generic method which must be reimplemented by derived classes.
     *
     * @param { string } queryString The query string to be executed
     *
     * @return { Promise<T> } Returns a {@link QueryResult}
     */
    abstract query<T extends QueryResult>( queryString: string ): Promise<T>;
}

