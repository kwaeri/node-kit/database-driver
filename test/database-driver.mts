/**
 * SPDX-PackageName: kwaeri/database-driver
 * SPDX-PackageVersion: 0.6.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
//import "@babel/polyfill";         // [ Resolves issues with "regeneratorRuntime NOT FOUND" <-- Always
import * as assert from 'assert'; //   use with generators in test sources (mocha/nyc/babel) ]
import { DriverConnectionBits, DatabaseDriver, BaseDatabaseDriver, QueryResult } from '../src/database-driver.mjs';


// DEFINES
const Derived = class DerivedDriver extends DatabaseDriver{
        constructor( config: DriverConnectionBits ) {
            super( config );
        }

        query( query: string ): Promise<any> {
            return Promise.resolve( { rows: [], fields: [] } )
        }
    },
    connectionBits: DriverConnectionBits = { host: "fake", port: 5000, database: "nonexistent", user: "not_real", password: "empty", type: "derived" },
    dbo = new Derived( connectionBits );


// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);

// Create table test
describe(
    'DatabaseDriver Functionality Test Suite',
    () => {

        describe(
            "DatabaseDriver 'type()' Test",
            () => {
                it(
                    'Should return true, indicating that the type assigned as expected.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                dbo.getType,
                                "derived"
                            )
                        );
                    }
                );
            }
        );

        describe(
            "DatabaseDriver 'connection()' Test",
            () => {
                it(
                    'Should return true, indicating that the connection bits assigned as expected.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( dbo.getConnection ),
                                JSON.stringify( { host: "fake", port: 5000, database: "nonexistent", user: "not_real", password: "empty" } )
                            )
                        );
                    }
                );
            }
        );


        describe(
            "DatabaseDriver 'query()' Test",
            () => {
                it(
                    'Should return true, indicating that the query retured the expected result.',
                    async () => {
                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( await dbo.query( "" ) ),
                                JSON.stringify( { rows: [], fields: [] } )
                            )
                        );
                    }
                );
            }
        );

    }
);
